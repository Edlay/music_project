class CreateDownloads < ActiveRecord::Migration[7.1]
  def change
    create_table :downloads do |t|
      t.belongs_to :song, foreign_key: true
      t.datetime :downloaded_at

      t.timestamps
    end
  end
end
