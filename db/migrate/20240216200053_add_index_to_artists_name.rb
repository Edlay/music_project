class AddIndexToArtistsName < ActiveRecord::Migration[7.1]
  def change
    add_index :artists, :name
  end
end
