class CreateSongs < ActiveRecord::Migration[7.1]
  def change
    create_table :songs do |t|
      t.string :title
      t.decimal :length, precision: 5, scale: 2
      t.integer :filesize
      t.belongs_to :artist, foreign_key: true

      t.timestamps
    end
  end
end
