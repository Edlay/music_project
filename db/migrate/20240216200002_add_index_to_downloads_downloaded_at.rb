class AddIndexToDownloadsDownloadedAt < ActiveRecord::Migration[7.1]
  def change
    add_index :downloads, :downloaded_at
  end
end
