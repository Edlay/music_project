class AddIndexToSongsTitle < ActiveRecord::Migration[7.1]
  def change
    add_index :songs, :title
  end
end
