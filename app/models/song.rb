class Song < ApplicationRecord
  belongs_to :artist
  has_many :downloads

  scope :ordered_by_title, -> { order(title: :asc) }
  scope :top, -> { joins(:downloads).group('songs.id').order('COUNT(downloads.id) DESC') }
  scope :top_in_period, -> (period, count) {
    joins(:downloads)
    .where(
      case period
      when 'day'
        { downloads: { downloaded_at: count.day.ago.beginning_of_day..Time.now } }
      when 'week'
        { downloads: { downloaded_at: count.week.ago.beginning_of_day..Time.now } }
      when 'month'
        { downloads: { downloaded_at: count.month.ago.beginning_of_day..Time.now } }
      else
        {}
      end
    )
    .group('songs.id')
    .order('COUNT(downloads.id) DESC')
  }
end
