class Artist < ApplicationRecord
  has_many :songs

  scope :top_by_initial_letter, -> (letter, count) {
    where('name LIKE ?', "#{letter}%")
      .joins(songs: :downloads)
      .group('artists.id')
      .order('COUNT(downloads.id) DESC')
      .limit(count)
  }
end
